## Press Pack / Press Kit

For **Kali Linux**, **Kali NetHunter** & **Kali NetHunter Pro** you should find:

- Logomark _(aka our dragon logos)_
- Logomark and Wordmark _(**our iconic avatars - dragon logo with text**)_
- Wordmark _(text as an image)_

With each of these, there are:

- Various different colour schemes _(blue, black, white & transparent etc)_
- Multiple image formats _(`*.png` & `*.jpg`)_
- An editable & scalable source _(`*.svg`)_

We recommend [Inkscape](https://inkscape.org/) when handling any `*.svg`.

See our [Graphic-Resources](https://gitlab.com/kalilinux/documentation/graphic-resources) for additional content _(such as banners and screenshots)_.

_Useful links: [Copyright & trademark information](https://www.kali.org/docs/policy/trademark/) and [press & media enquiries](https://www.kali.org/contact/)_

[![Kali Linux](Kali/Logomark_and_Wordmark/kali-logo-dragon-blue-transparent.png)](Kali/)
[![Kali NetHunter](Kali_NetHunter/Logomark_and_Wordmark/kali-nethunter-logo-dragon-grey-transparent.png)](Kali_NetHunter/)
[![Kali NetHunter Pro](Kali_NetHunter_Pro/Logomark_and_Wordmark/kali-nethunterpro-logo-dragon-orange-transparent.png)](Kali_NetHunter_Pro/)

- - -

### What Is Kali Linux?

**Kali Linux: The Most Advanced Penetration Testing Distribution. Ever**

_The Quieter You Become, The More You Are Able To Hear._

[Kali Linux](https://www.kali.org/) _(formerly known as [BackTrack Linux](https://www.backtrack-linux.org/))_ is an [open-source](https://www.kali.org/docs/policy/kali-linux-open-source-policy/), [Debian-based Linux](https://www.kali.org/docs/policy/kali-linux-relationship-with-debian/) distribution aimed at advanced Penetration Testing and Security Auditing. It [does this by](https://www.kali.org/features/) providing common tools, configurations, and automations which allows the user to focus on the task that needs to be completed, not the surrounding activity.

Kali Linux contains industry specific modifications as well as [several hundred tools](https://www.kali.org/docs/policy/penetration-testing-tools-policy/) targeted towards various Information Security tasks, such as Penetration Testing, Security Research, Computer Forensics, Reverse Engineering, Vulnerability Management and Red Team Testing.

Kali Linux is a multi-platform solution, accessible and freely available to information security professionals and hobbyists.

```
       ...:::---===++++*****++=--:.
                            .:--=+*%%%#+=-.
                                      .-=*#@%#+-.
                    .::---====++++++====--:::=+#@-
         .:-=++*****+++===------:------===+**#%-@+
  ...::---:.                          .:--==++++=%
                              :-=*#%@@%#**++==--.+#            .
                        :=*#%#*+=-:.              ##:           ..::
                   :=*#*+-:                       .@@@####*++=:.  . :--
               -+++=:                           :*@@@%#****#%@@@@%%=.= -=:
           :-=-.                              .#@@*-           .-+#@@@%=*:=-
         .:                                  -@@#.                  .-+#@@*++.
                                            .@@#                         =@@@#:
                                            +@@:                          =#@@@:
                                            #@@.                             :+@#+-
                                            #@@=                                =+.
                                            -@@@.
                                             =@@@-
                                              :%@@#-
                                                -#@@@#+=:..
                                                   -+#%@@@@@@@@@@@@%%##*+=-.
                                                           ...:::-==+#%@@@%*##*=:
                                                                        .-*%#=.:+#*-
                                                                            .+@+  .=#+
                                                                              .*@-   -#-
                                                                                -@=    ++
                                                                                 .%+    --
                                                                                   %-    -
                                                                                   .%
                                                                                    ==
                                                                                     *
                                                                                     =
                                                                                     ..
```
